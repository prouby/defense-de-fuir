# Jamming assembly projet

 - Team: PUTACLIC 2 - Le retour
 - Date: 8-10 novembre 2019
 - Made in **Lua** with LÖVE **11.2** (https://love2d.org/)


## The team

 * Nicolas MARIN-PACHE
 * Pierre-Antoine ROUBY
 * Pamphile SALTEL
 * Rhenaud DUBOIS
 * Pauline WARGNY

## Associates

https://github.com/ROMAINPC/Sound-Jam-PUTACLIC


## Get love

### Linux

Install from official repo on Debian-like.

```shell
sudo add-apt-repository ppa:bartbes/love-stable
sudo apt-get update
sudo apt-get install love
love --version
```

Install on GuixSD

```shell
guix install love
love --version
```

Install from guix local pack tar.

```shell
wget https://parouby.fr/pub/love-env.tar.gz
tar xf love-env.tar.gz
./mybin/love --version
```


### Mac & window

You're on your own.


## How to play

Run from source directory.

```shell
cd defense-de-fuir/
love .
```

Run from love archive.

```shell
love defense_de_fuir.love
```
