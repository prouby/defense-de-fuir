-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

return {
    -- Initialise the animation data and load the image
    LoadAnim = function(entityData)
        local anim = {
            state = 0,
            variation = 0,
            nbSprite = entityData.nbSprite,
            wait = 0,
            run = entityData.run
        }
        if love.filesystem.getInfo(entityData.anim.."Up.png") ~= nil then
            anim.imageup = love.graphics.newImage(entityData.anim.."Up.png")
            anim.nbColumnup = anim.imageup:getPixelWidth() / Map.TILE_SIZE
        end
        if love.filesystem.getInfo(entityData.anim.."Down.png") ~= nil then
            anim.imagedown = love.graphics.newImage(entityData.anim.."Down.png")
            anim.nbColumndown = anim.imagedown:getPixelWidth() / Map.TILE_SIZE
        end
        if love.filesystem.getInfo(entityData.anim.."Left.png") ~= nil then
            anim.imageleft = love.graphics.newImage(entityData.anim.."Left.png")
            anim.nbColumnleft = anim.imageleft:getPixelWidth() / Map.TILE_SIZE
        end
        if love.filesystem.getInfo(entityData.anim.."Right.png") ~= nil then
            anim.imageright = love.graphics.newImage(entityData.anim.."Right.png")
            anim.nbColumnright = anim.imageright:getPixelWidth() / Map.TILE_SIZE
        end
        if entityData.anim ~= "" and not anim.imageright and not anim.imageup and not anim.imageleft and not anim.imagedown then
            anim.imagedown = love.graphics.newImage(entityData.anim..".png")
            anim.imageup = anim.imagedown
            anim.imageleft = anim.imagedown
            anim.imageright = anim.imagedown
            anim.nbColumnup = anim.imagedown:getPixelWidth() / Map.TILE_SIZE
            anim.nbColumndown = anim.nbColumnup
            anim.nbColumnright = anim.nbColumnup
            anim.nbColumnleft = anim.nbColumnup
        end

        return anim
    end,
    -- Create entity from entityData
    Create = function(entityData)
        local entity = {
            entityData = entityData,
            anim = Entity.LoadAnim(entityData),
            moveOffsetX = 0,
            moveOffsetY = 0
        }
        entity.Draw = Entity.Draw
        entity.CanMove = Entity.CanMove
        entity.IsMoving = Entity.IsMoving
        return entity
    end,
    CanMove = function(entity, dir)
        return not entity.entityData.block and not (
            (dir == "up" and entity.entityData.blockNorth) or
            (dir == "down" and entity.entityData.blockSouth) or
            (dir == "right" and entity.entityData.blockEast) or
            (dir == "left" and entity.entityData.blockWest))
    end,
    -- Draw the current sprite of the animation
    Draw = function(self, map)
        if self.carry then
            self.entityData.dir = self.carry.entityData.dir
            self.moving = self.carry.moving
            self.moveOffsetX = self.carry.moveOffsetX
            self.moveOffsetY = self.carry.moveOffsetY - 13
        end
        -- Check if image exists
        if self.anim["image"..self.entityData.dir] == nil then
            return 1
        end
        -- Reset state at the end of the animation
        if self.anim.state >= self.anim.nbSprite[self.anim.variation + 1] then
            self.anim.state = 0
        end
        -- Create quadrilateral
        local quad = love.graphics.newQuad(
            self.anim.state % self.anim["nbColumn"..self.entityData.dir]
                * Map.TILE_SIZE * self.entityData.size.w,
            self.anim.variation
                * Map.TILE_SIZE * self.entityData.size.h,
            Map.TILE_SIZE * self.entityData.size.w,
            Map.TILE_SIZE * self.entityData.size.h,
            self.anim["image"..self.entityData.dir]:getDimensions()
        )

        local x, y = map:GetTopLeft()

        posX = self.entityData.pos.x * Map.TILE_SIZE
        posY = self.entityData.pos.y * Map.TILE_SIZE
        if self.moving then
            posX = posX + self.moveOffsetX
            posY = posY + self.moveOffsetY
        elseif self.carry then
            posY = posY - 13
        end
        -- Draw quadrilateral
        love.graphics.draw(self.anim["image"..self.entityData.dir], quad,
            posX * Map.TILE_SCALE + x,
            posY * Map.TILE_SCALE + y,
            0, Map.TILE_SCALE, Map.TILE_SCALE)
        -- Pause if anim is paused
        if self.anim.run then
            if self.anim.wait >= self.entityData.animWait then
                self.anim.state = self.anim.state + 1
                self.anim.wait = 0
            else
                self.anim.wait = self.anim.wait + 1
            end
        end
    end,
    IsMoving = function(entity)
        return entity.moveOffsetX ~= 0 or entity.moveOffsetY ~= 0
    end
}
