-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

return {
    TILE_SIZE = 16, -- 16x16px tiles
    TILE_SCALE = 4,
    Create = function(mapName)
        local map = require ("data/Scripts/Maps/" .. mapName)

        -- Shortcuts
        map.CreateTiles = Map.CreateTiles
        map.CanMove = Map.CanMove
        map.DiscoverRoom = Map.DiscoverRoom
        map.GetEventAt = Map.GetEventAt
        map.GetTopLeft = Map.GetTopLeft
        map.GetEntityByName = Map.GetEntityByName
        map.Draw = Map.Draw
        map.StartCutscene = Map.StartCutscene
        map.StopCutscene = Map.StopCutscene
        map.HasCutscene = Map.HasCutscene
        map.Destroy = Map.Destroy
        map.EventUpdate = Map.EventUpdate
        map.Finish = Map.Finish

        map.finish = false
        map.music = mapName..".mp3"

        -- Tileset creation
        map.tileset = Tileset.Create(mapName)

        -- Tile creation
        map:CreateTiles()

        -- Entity creation
        for k, v in pairs(map.entities) do
            map.entities[k] = Entity.Create(v)
        end
        if map.keys then
            local i = love.math.random(11)
            --print(i, map.keys[i])
            map.key = map.keys[i]
        end
        return map
    end,
    CreateTiles = function(map)
        map.spriteMap = {}
        for y, line in pairs(map.tilemap) do
            map.spriteMap[y] = {}
            for x, tile in pairs(line) do
                if love.filesystem.getInfo(map.tileset.tilefolder .. map.tileset.tileset[tile].img .. ".png") ~= nil and map.tileset.tileset[tile].img ~= nil then
                    map.spriteMap[y][x] = love.graphics.newImage(map.tileset.tilefolder .. map.tileset.tileset[tile].img .. ".png")
                else
                    map.spriteMap[y][x] = love.graphics.newImage("data/Sprites/default.png")
                end
            end
        end
    end,
    CanMove = function(map, x, y, dir, ignoreOrigin)
        if dir ~= "left" and dir ~= "right" and dir ~= "up" and dir ~= "down" then
            return false
        end
        local invDir = Util.GetReverseDir(dir)

        x = x + 1
        y = y + map:GetEntityByName("Player").entityData.size.h

        local destX = x + (dir == "left" and 1 or dir == "right" and -1 or 0)
        local destY = y + (dir == "up"   and 1 or dir == "down"  and -1 or 0)
        --print(map.tileset.tileset[map.tilemap[destY][destX]].img)
        if not map.tileset:CanMove(map.tilemap[y][x], dir) or
           not map.tileset:CanMove(map.tilemap[destY][destX], invDir) then
            return false
        end

        if not ignoreOrigin then
            local event = map:GetEventAt(x - 1, y - 1)
            if event then
                if not event:CanMove(dir) then
                    return false
                end
            end
        end

        local nextEvent = map:GetEventAt(destX - 1, destY - 1)
        if nextEvent then
            if not nextEvent:CanMove(invDir) then
                --print(nextEvent.entityData.name)
                return false
            end
        end

        return true
    end,
    DiscoverRoom = function(map, zone)
        -- TODO
    end,
    GetEventAt = function(map, x, y)
        for k, v in pairs(map.entities) do
            if v.entityData.pos.x == x and v.entityData.pos.y == y then
                return v
            end
        end
        return nil
    end,
    Draw = function(map)
        if Music.currentMusic ~= map.music then
            -- print (map.music)
            -- print (Music.currentMusic)
            if Music.currentMusic ~= nil then
                Music:Stop(Music.currentMusic)
            end
            Music.currentMusic = map.music
            Music:Play(map.music, true)
        end

        local mapX, mapY = map:GetTopLeft()
        for y, line in pairs(map.spriteMap) do
            local drawY = mapY + (y - 1) * Map.TILE_SIZE * Map.TILE_SCALE
            for x, tile in pairs(line) do
                local drawX = mapX + (x - 1) * Map.TILE_SIZE * Map.TILE_SCALE
                local tile = map.spriteMap[y][x]
                love.graphics.draw(tile, drawX, drawY, 0, Map.TILE_SCALE)
            end
        end

        local hasCutscene = map:HasCutscene()
        for k, event in pairs(map.entities) do
            if map[event.entityData.funcPrefix.."Update"] then
                map[event.entityData.funcPrefix.."Update"](event, map)
            end
            event:Draw(map)
        end

        if map:HasCutscene() then
            map:cutscene(map.cutsceneFrame)
            map.cutsceneFrame = map.cutsceneFrame + 1
            return
        end

        if map.EventUpdate then
            map:EventUpdate()
        end

        if dialogList[1] then
            Util.DrawDialogBox(dialogList[1])
        end
    end,
    EventUpdate = function(map)
        local player = map:GetEntityByName("Player")
        local x = player.entityData.pos.x
        local y = player.entityData.pos.y
        if input.action == input.PRESSED
            or input.left == input.PRESSED
            or input.up == input.PRESSED
            or input.right == input.PRESSED
            or input.down == input.PRESSED then
                if dialogList[1] then
                    table.remove(dialogList, 1)
                end
        end

        if input.action == input.PRESSED then
            local dir = player.entityData.dir
            local x = player.entityData.pos.x
            local y = player.entityData.pos.y

            if player.carries and map:CanMove(x, y, dir) then
                if dir == "up" and map:GetEventAt(x, y - 1) == nil then
                    if player.moving then
                        player.entityData.y = y + 1
                        player.moving = false
                        player.moveOffsetX = 0
                        player.moveOffsetY = 0
                    end
                    player.carries.entityData.pos = {x = x, y = y - 1}
                    player.carries.entityData.block = true
                    player.carries.moveOffsetX = 0
                    player.carries.moveOffsetY = 0
                    player.carries.carry = nil
                    player.carries = nil
                elseif dir == "down" and map:GetEventAt(x, y + 1) == nil then
                    if player.moving then
                        player.entityData.y = y - 1
                        player.moving = false
                        player.moveOffsetX = 0
                        player.moveOffsetY = 0
                    end
                    player.carries.entityData.pos = {x = x, y = y + 1}
                    player.carries.entityData.block = true
                    player.carries.moveOffsetX = 0
                    player.carries.moveOffsetY = 0
                    player.carries.carry = nil
                    player.carries = nil
                elseif dir == "left" and map:GetEventAt(x - 1, y) == nil then
                    if player.moving then
                        player.entityData.x = x + 1
                        player.moving = false
                        player.moveOffsetX = 0
                        player.moveOffsetY = 0
                    end
                    player.carries.entityData.pos = {x = x - 1, y = y}
                    player.carries.entityData.block = true
                    player.carries.moveOffsetX = 0
                    player.carries.moveOffsetY = 0
                    player.carries.carry = nil
                    player.carries = nil
                elseif dir == "right" and map:GetEventAt(x + 1,y) == nil then
                    if player.moving then
                        player.entityData.x = x - 1
                        player.moving = false
                        player.moveOffsetX = 0
                        player.moveOffsetY = 0
                    end
                    player.carries.entityData.pos = {x = x + 1, y = y}
                    player.carries.entityData.block = true
                    player.carries.moveOffsetX = 0
                    player.carries.moveOffsetY = 0
                    player.carries.carry = nil
                    player.carries = nil
                end
                return
            end
            y = y + (player.entityData.size.h - 1)
            if dir == "up"        then y = y - 1
            elseif dir == "down"  then y = y + 1
            elseif dir == "left"  then x = x - 1
            elseif dir == "right" then x = x + 1 end

            for k, ent in pairs(map.entities) do
                if ent.entityData.pos.x == x and ent.entityData.pos.y == y then
                    if map[ent.entityData.funcPrefix.."Action"] then
                        map[ent.entityData.funcPrefix.."Action"](ent, map)
                    end
                end
            end
        end
        entityDown = map:GetEventAt(x, y + 1)
        if entityDown ~= nil and entityDown.entityData.size.h == 2 then
            entityDown:Draw(map)
        end
    end,
    GetTopLeft = function(map)
        local player = map:GetEntityByName("Player")
        if player == nil then return 0, 0 end

        local retX = 300 - (player.entityData.pos.x * Map.TILE_SIZE + player.moveOffsetX) * Map.TILE_SCALE
        local retY = 220 - (player.entityData.pos.y * Map.TILE_SIZE + player.moveOffsetY) * Map.TILE_SCALE
        return retX, retY
    end,
    GetEntityByName = function(map, entityName)
        for _, event in pairs(map.entities) do
            if event.entityData.name == entityName then
                return event
            end
        end
        return nil
    end,
    StartCutscene = function(map, func)
        map.cutscene = map[func]
        map.cutsceneFrame = 0
    end,
    StopCutscene = function(map)
        map.cutscene = nil
    end,
    Finish = function(map)
        Map.TILE_SCALE = Map.TILE_SCALE - 1
        if map.nextmap == nil then
            gameState = CREDIT
        else
            currentMap = maps[map.nextmap]
        end
        map.finish = true
    end,
    HasCutscene = function(map)
        return map.cutscene ~= nil
    end,
    Destroy = function(map)
        map.tileset = nil
        -- TODO
        --[[for k, v in pairs(map.entities) do
            Entity.Destroy(v)
        end]]
        for y = 1, y <= #map.spriteMap do
            for x = 1, x <= #map.spriteMap[y] do
                map.spriteMap[y][x]:release()
            end
        end
        map = nil
    end
}
