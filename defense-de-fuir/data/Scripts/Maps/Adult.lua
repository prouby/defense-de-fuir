-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

return {
    nextmap = "Old",
    tilemap = {
        { 8, 6, 6, 2, 2, 6, 6,13, 4, 4, 4, 4, 4, 4, 4, 2, 4, 9 },
        { 8, 2, 2, 2, 2, 2, 2,10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 2, 2, 2, 2, 2, 2,10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 2, 2, 2, 2, 2, 2,10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 2, 2, 2, 2, 2, 2, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 5, 5, 5, 5, 5, 5,13, 4, 4, 4, 4, 1, 1, 4, 4, 4, 9 },
        { 8, 3, 3, 3, 3, 3, 3, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 3, 3, 3, 3, 3, 3,13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 3, 3, 3, 3, 3, 3,10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 4, 4, 4, 4, 4, 4, 4, 4,13, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 1, 1, 1, 1, 1, 1, 1, 1,13, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 1, 1, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 1, 1, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 1, 9 },
        { 8, 1, 1, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 1, 9 },
        {14, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,14 },
    },
    keys = {
        "Etabli",
        "Frigo",
        "Bar",
        "Poubelle",
        "Manteau",
        "Table",
        "Canape",
        "Linge",
        "Bureau",
        "Lit",
        "Fleur",
        "WC"
    },
    zones = {
    },
    playerSpawn = { x = 7, y = 6 },
    entities = {
        {
            name = "Garage",
            funcPrefix = "Garage",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/garagedoor",
            nbSprite = {2},
            animWait = 30,
            pos = { x = 3, y = 0 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "GarageFantom",
            funcPrefix = "Garage",
            funcPage = 1,
            anim = "",
            nbSprite = {0},
            animWait = 0,
            pos = { x = 4, y = 0 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "DoorTopRight",
            funcPrefix = "DoorTR",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/door",
            nbSprite = {2},
            animWait = 30,
            pos = { x = 15, y = 0 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Etabli",
            funcPrefix = "Etabli",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/etabli",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 4 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "EtabliF",
            funcPrefix = "Etabli",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 5 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Voiture",
            funcPrefix = "Voiture",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/car",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 3, y = 2 },
            size = { w = 2, h = 3 },
            dir = "down",
            block = true
        },
        {
            name = "VoitureF",
            funcPrefix = "Voiture",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 3, y = 3 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "VoitureF",
            funcPrefix = "Voiture",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 3, y = 4 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "VoitureF",
            funcPrefix = "Voiture",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 4, y = 2 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "VoitureF",
            funcPrefix = "Voiture",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 4, y = 3 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "VoitureF",
            funcPrefix = "Voiture",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 4, y = 4 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Cuisiniere",
            funcPrefix = "Cuisiniere",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/cuisiniere",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 8, y = 0 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "CuisiniereF",
            funcPrefix = "Cuisiniere",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 8, y = 1 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Frigo",
            funcPrefix = "Frigo",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/frigogidaire",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 0 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "FrigoF",
            funcPrefix = "Frigo",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 1 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/lavabo",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 10, y = 0 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "LavaboF",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 10, y = 1 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Meuble",
            funcPrefix = "Meuble",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/meuble",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 11, y = 0 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "MeubleF",
            funcPrefix = "Meuble",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 11, y = 1 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bin",
            funcPrefix = "Bin",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/bin",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 13, y = 1 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Portemanteau",
            funcPrefix = "Portemanteau",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/portemanteau",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 16, y = 3 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = false
        },
        {
            name = "PortemanteauF",
            funcPrefix = "Portemanteau",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 16, y = 4 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Table",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/table",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 10, y = 8 },
            size = { w = 2, h = 3 },
            dir = "down",
            block = true
        },
        {
            name = "Table",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 10, y = 9 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TableF",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 10, y = 10 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TableF",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 11, y = 8 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TableF",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 11, y = 9 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TableF",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 11, y = 10 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Chair1",
            funcPrefix = "Chair",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/chair",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 9 },
            size = { w = 1, h = 1 },
            dir = "right",
            block = true
        },
        {
            name = "Chair2",
            funcPrefix = "Chair",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/chair",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 10 },
            size = { w = 1, h = 1 },
            dir = "right",
            block = true
        },
        {
            name = "Chair3",
            funcPrefix = "Chair",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/chair",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 12, y = 8 },
            size = { w = 1, h = 1 },
            dir = "left",
            block = true
        },
        {
            name = "Chair4",
            funcPrefix = "Chair",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/chair",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 12, y = 10 },
            size = { w = 1, h = 1 },
            dir = "left",
            block = true
        },
        {
            name = "Canape",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/canape",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 12, y = 13 },
            size = { w = 4, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "CanapeF",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 13, y = 13 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "CanapeF",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 14, y = 13 },
            size = { w = 4, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "CanapeF",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 15, y = 13 },
            size = { w = 4, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Bar",
            funcPrefix = "Bar",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/bar",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 8, y = 4 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bar2",
            funcPrefix = "Bar",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/bar",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 4 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bar3",
            funcPrefix = "Bar",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/bar",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 10, y = 4 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bar4",
            funcPrefix = "Bar",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/bar",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 11, y = 4 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TableB",
            funcPrefix = "TableB",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/smoltable",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 13, y = 15 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TableBF",
            funcPrefix = "TableB",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 14, y = 15 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/shower",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 6 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "ShowerF",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 7 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Bath",
            funcPrefix = "Bath",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/bath",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 10 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "BathF",
            funcPrefix = "Bath",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 2, y = 10 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Linge",
            funcPrefix = "Linge",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/corbeillealinge",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 6, y = 7 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/bigbed",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 14 },
            size = { w = 2, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "BedF",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 2, y = 14 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "BedF",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 15 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "BedF",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 2, y = 15 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bedshelf",
            funcPrefix = "Bedshelf",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/tabledenuit",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 12 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = false
        },
        {
            name = "BedshelfF",
            funcPrefix = "BedshelfF",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 13 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bedshelf2",
            funcPrefix = "Bedshelf2",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/tabledenuit",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 16 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = false
        },
        {
            name = "Bedshelf2F",
            funcPrefix = "Bedshelf2F",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 17 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo2",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/lavabo",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 5, y = 6 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Desk",
            funcPrefix = "Desk",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/desk",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 5, y = 11 },
            size = { w = 2, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "DeskF",
            funcPrefix = "Desk",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 5, y = 7 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "DeskF",
            funcPrefix = "Desk",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 5, y = 12 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "DeskF",
            funcPrefix = "Desk",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 6, y = 11 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "DeskF",
            funcPrefix = "Desk",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 6, y = 12 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Chair5",
            funcPrefix = "Chair5",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/chairUp",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 5, y = 13 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Plant",
            funcPrefix = "Plant",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/small_plant",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 8, y = 17 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Toilets",
            funcPrefix = "Toilets",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/toilets",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 6, y = 10 },
            size = { w = 1, h = 1 },
            dir = "left",
            block = true
        },
        {
            name = "Player",
            funcPrefix = "Player",
            funcPage = 1,
            anim = "data/Sprites/Characters/Player/Adult/",
            nbSprite = {2, 4},
            animWait = 25,
            pos = { x = 2, y = 1 },
            size = { w = 1, h = 2 },
            dir = "down",
            speed = 2,
            run = true
        },
    },



    ChestAction = function(ent, map)
        ent.anim.state = 1
    end,

    KeyStuff = function(map, keyword)
        if map.key == keyword then
            map.key = true
            Music:Play("son-cle-"..math.random(1,2)..".wav", false)
            table.insert(dialogList, "Vous avez trouvé la clef !")
            return true
        else
            Music:Play("adulte-"..math.random(1,3)..".wav", false)
            table.insert(dialogList, "C'est vide.")
            return false
        end
    end,

    EtabliAction = function(entity, map)       map:KeyStuff("Etabli")   end,
    FrigoAction = function(entity, map)        map:KeyStuff("Frigo")    end,
    BarAction = function(entity, map)          map:KeyStuff("Bar")      end,
    BinAction = function(entity, map)          map:KeyStuff("Poubelle") end,
    PortemanteauAction = function(entity, map) map:KeyStuff("Manteau")  end,
    TableAction = function(entity, map)        map:KeyStuff("Table")    end,
    CanapeAction = function(entity, map)       map:KeyStuff("Canape")   end,
    LingeAction = function(entity, map)        map:KeyStuff("Linge")    end,
    DeskAction = function(entity, map)         map:KeyStuff("Bureau")   end,
    BedAction = function(entity, map)          map:KeyStuff("Lit")      end,
    PlantAction = function(entity, map)        map:KeyStuff("Fleur")    end,
    ToiletsAction = function(entity, map)      map:KeyStuff("WC")       end,

    VoitureAction = function(entity, map)
        if map.key == true then
            map:StartCutscene("EndCutScene")
        else
            Music:Play("adulte-"..math.random(1,3)..".wav", false)
            table.insert(dialogList, "La clef est introuvable.")
        end
    end,

    EndCutScene = function(map, frame)
        if frame == 0 then
            Music:Play("voiture-short-"..math.random(1,4)..".wav", false)
        end

        if frame == 30 then
            local garage = map:GetEntityByName("Garage")
            garage.anim.state = 1
        end

        if frame == 120 then
            map:StopCutscene()
            map:Finish()
        end

    end,

    PlayerUpdate = function(player, map)
        if not player.moving then
            if input.up == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "down") then
                player.entityData.dir = "up"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = 0
                player.moveOffsetY = -1
            elseif input.down == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "up") then
                player.entityData.dir = "down"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = 0
                player.moveOffsetY = 1
            elseif input.right == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "left") then
                player.entityData.dir = "right"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = 1
                player.moveOffsetY = 0
            elseif input.left == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "right") then
                player.entityData.dir = "left"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = -1
                player.moveOffsetY = 0
            else
                if input.up == input.HELD then
                    player.entityData.dir = "up"
                elseif input.down == input.HELD then
                    player.entityData.dir = "down"
                elseif input.left == input.HELD then
                    player.entityData.dir = "left"
                elseif input.right == input.HELD then
                    player.entityData.dir = "right"
                end
                player.anim.variation = 0
            end
        elseif player.moveOffsetX >= 16 or player.moveOffsetY >= 16 or player.moveOffsetX <= -16 or player.moveOffsetY <= -16 then
            player.moving = false
            if player.moveOffsetX > 0 then
                player.entityData.pos.x = player.entityData.pos.x + 1
            elseif player.moveOffsetX < 0 then
                player.entityData.pos.x = player.entityData.pos.x - 1
            elseif player.moveOffsetY > 0 then
                player.entityData.pos.y = player.entityData.pos.y + 1
            elseif player.moveOffsetY < 0 then
                player.entityData.pos.y = player.entityData.pos.y - 1
            end
            player.moveOffsetX = 0
            player.moveOffsetY = 0
        else
            --TODO: simplify logic
            if player.moveOffsetX > 0 then
                player.moveOffsetX = player.moveOffsetX + player.entityData.speed
            elseif player.moveOffsetX < 0 then
                player.moveOffsetX = player.moveOffsetX - player.entityData.speed
            elseif player.moveOffsetY > 0 then
                player.moveOffsetY = player.moveOffsetY + player.entityData.speed
            elseif player.moveOffsetY < 0 then
                player.moveOffsetY = player.moveOffsetY - player.entityData.speed
            end
        end
    end
}
