-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

return {
    nextmap = "Adult",
    tilemap = {
        { 4, 2, 2, 1, 2, 2, 5 },
        { 4, 1, 1, 1, 1, 1, 5 },
        { 4, 1, 1, 1, 1, 1, 5 },
        { 4, 1, 1, 1, 1, 1, 5 },
        { 4, 1, 1, 1, 1, 1, 5 },
        { 4, 1, 1, 1, 1, 1, 5 },
        {10, 3, 3, 3, 3, 3,10 }
    },
    zones = {
        landeau = {
            { x = 7, y = 7 },
            { x = 8, y = 7 }
        }, -- 2x1
        salle = {
            locked = true,
            { x = 1, y = 1 },
            { x = 9, y = 1 },
            { x = 9, y = 9 },
            { x = 1, y = 9 }
        }  -- 9x9, inclut landeau
    },
    playerSpawn = { x = 7, y = 6 },
    entities = {
        {
            name = "Door",
            funcPrefix = "Door",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/door",
            nbSprite = {1, 1},
            animWait = 30,
            pos = { x = 3, y = 0 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Chest",
            funcPrefix = "Chest",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/chest",
            nbSprite = {2},
            animWait = 30,
            pos = { x = 1, y = 3 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Chair",
            funcPrefix = "Chair",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/chair",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 4, y = 2 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Crib",
            funcPrefix = "Crib",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/baby_bed",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 4, y = 4 },
            size = { w = 2, h = 2 },
            dir = "down"
        },
        {
            name = "Teddy",
            funcPrefix = "Teddy",
            funcPage = 1,
            anim = "data/Sprites/Accessories/teddy",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 500, y = 500 },
            size = { w = 1, h = 1 },
            dir = "down"
        },
        {
            name = "Player",
            funcPrefix = "Player",
            funcPage = 1,
            anim = "data/Sprites/Characters/Player/Baby/",
            nbSprite = {2, 4},
            animWait = 30,
            pos = { x = 5, y = 4 },
            size = { w = 1, h = 1 },
            dir = "down",
            speed = 1,
            run = true
        },
    },

    ChestAction = function(ent, map)
        if ent.anim.state ~= 1 then
            Music:Play("porte-ouvre-"..math.random(1,2)..".wav", false)
            ent.anim.state = 1
            map:GetEntityByName("Teddy").entityData.pos = {x = 2, y = 3}
        end
    end,

    ChairCutscene = function(map, frame)
        local player = map:GetEntityByName("Player")

        if frame == 0 then
            player.entityData.pos = {x = 3, y = 2}
            player.entityData.dir = "up"
        elseif frame <= 16 then
            player.anim.variation = 1
            player.moving = true
            player.moveOffsetX = 0
            player.moveOffsetY = -1
        elseif frame == 30 then
            Music:Play("porte-ouvre-"..math.random(1,2)..".wav", false)
            local door = map:GetEntityByName("Door")
            door.anim.variation = 1
        elseif frame > 46 and frame <= 62 then
            player.anim.variation = 1
            player.moving = true
            player.moveOffsetX = 0
            player.moveOffsetY = -1
        elseif frame == 120 then
            player.entityData.pos = {x = 3, y = 0}
            map:StopCutscene()
            map:Finish()
        end
    end,

    DoorAction = function(entity, map)
        Music:Play("bebe-"..math.random(1,4)..".wav", false)
        table.insert(dialogList, "Moi: Zé tro haut!")
    end,

    ChairAction = function(entity, map)
        entity.anim.state = 1
        local door = map:GetEntityByName("Door")
        if not door then print("NO DOOR HELP") return end

        -- Opens the door
        if entity.entityData.pos.x == door.entityData.pos.x and entity.entityData.pos.y == door.entityData.pos.y + 1 then
            map:StartCutscene("ChairCutscene")
        else
            entity.entityData.block = false
            local p = map:GetEntityByName("Player")
            entity.entityData.pos = p.entityData.pos
            entity.carry = p
            p.carries = entity
        end
    end,

    TeddyAction = function(entity, map)
        Music:Play("teddy-bear-2.wav", false)
        table.insert(dialogList, "Teddy: Pouiiitt!")
    end,

    PlayerUpdate = function(player, map)
        if not player.moving then
            if input.up == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "down") then
                player.entityData.dir = "up"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = 0
                player.moveOffsetY = -1
            elseif input.down == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "up") then
                player.entityData.dir = "down"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = 0
                player.moveOffsetY = 1
            elseif input.right == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "left") then
                player.entityData.dir = "right"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = 1
                player.moveOffsetY = 0
            elseif input.left == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "right") then
                player.entityData.dir = "left"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = -1
                player.moveOffsetY = 0
            else
                if input.up == input.HELD then
                    player.entityData.dir = "up"
                elseif input.down == input.HELD then
                    player.entityData.dir = "down"
                elseif input.left == input.HELD then
                    player.entityData.dir = "left"
                elseif input.right == input.HELD then
                    player.entityData.dir = "right"
                end
                player.anim.variation = 0
            end
        elseif player.moveOffsetX >= 16 or player.moveOffsetY >= 16 or player.moveOffsetX <= -16 or player.moveOffsetY <= -16 then
            player.moving = false
            if player.moveOffsetX > 0 then
                player.entityData.pos.x = player.entityData.pos.x + 1
            elseif player.moveOffsetX < 0 then
                player.entityData.pos.x = player.entityData.pos.x - 1
            elseif player.moveOffsetY > 0 then
                player.entityData.pos.y = player.entityData.pos.y + 1
            elseif player.moveOffsetY < 0 then
                player.entityData.pos.y = player.entityData.pos.y - 1
            end
            player.moveOffsetX = 0
            player.moveOffsetY = 0
        else
            --TODO: simplify logic
            if player.moveOffsetX > 0 then
                player.moveOffsetX = player.moveOffsetX + player.entityData.speed
            elseif player.moveOffsetX < 0 then
                player.moveOffsetX = player.moveOffsetX - player.entityData.speed
            elseif player.moveOffsetY > 0 then
                player.moveOffsetY = player.moveOffsetY + player.entityData.speed
            elseif player.moveOffsetY < 0 then
                player.moveOffsetY = player.moveOffsetY - player.entityData.speed
            end
        end
    end
}
