-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

return {
    nextmap = nil,
    tilemap = {
      --  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29
        { 8, 4, 4, 4, 4, 4,13, 4, 4, 4,13, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 4, 4, 4, 4, 4, 4, 9},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1, 4, 4, 1, 4, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,13, 1, 1,13, 1, 1, 1, 1, 1, 1,13, 4, 1, 4, 9},
        { 8, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 1, 1,10, 1, 1,10, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,10, 1, 1,10, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1,13, 4, 1, 4,13, 1, 1, 1, 1,10, 1, 1,10, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1,10, 4, 4, 4, 4, 4, 4,13, 4, 1, 4, 4, 4, 4, 9},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 9},
        { 8, 4, 4, 4, 4, 4,10, 4, 4, 4,10, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,13, 1, 1, 1, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1,13, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1, 4, 4, 1, 4, 4, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 9},
        { 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1,10, 1, 1, 1, 1, 1, 1, 9},
        { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,13, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 9},
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9},
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9},
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9},
        { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,13, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9},
        {11, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 1, 1,12, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,14},
        { 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1,13, 4, 1, 4,13, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 4, 4, 4, 4, 4,10, 4, 4, 4,10, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1, 4, 4, 1, 4, 4, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1,13, 4, 1, 4,13, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        { 8, 1, 1, 1, 1, 1,10, 1, 1, 1,10, 1, 1, 1, 1, 9,14,14,14,14,14,14,14,14,14,14,14,14,14,14},
        {14, 7, 7, 7, 7, 7,14, 7, 7, 7,14, 7, 7, 7, 7,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14}
    },
    zones = {
    },
    playerSpawn = { x = 7, y = 6 },
    entities = {

        --CHAMBRE 1
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/old_bed",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 3 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 2, y = 3 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TV",
            funcPrefix = "TV",
            funcPage = 1,
            anim = "data/Sprites/Accessories/TV",
            nbSprite = {9},
            animWait = 15,
            pos = { x = 3, y = 0 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "Perf",
            funcPrefix = "Perf",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/perfusion",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 2 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Table",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/tableroulante",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 5, y = 3 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/shower",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 0 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 1 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Toilets",
            funcPrefix = "Toilets",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/toilets",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 3 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/lavaboDown",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 0},
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 1 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },


        -- CHAMBRE 2
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/old_bed",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 9 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 2, y = 9 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TV",
            funcPrefix = "TV",
            funcPage = 1,
            anim = "data/Sprites/Accessories/TV",
            nbSprite = {9},
            animWait = 15,
            pos = { x = 3, y = 6 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "Perf",
            funcPrefix = "Perf",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/perfusion",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 8 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Table",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/tableroulante",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 5, y = 9 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/shower",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 8 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 9 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Toilets",
            funcPrefix = "Toilets",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/toilets",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 11 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/lavaboDown",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 8},
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 8 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },

        --CHAMBRE 3
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/old_bed",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 15 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 2, y = 15 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TV",
            funcPrefix = "TV",
            funcPage = 1,
            anim = "data/Sprites/Accessories/TV",
            nbSprite = {9},
            animWait = 15,
            pos = { x = 3, y = 12 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "Perf",
            funcPrefix = "Perf",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/perfusion",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 14 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Table",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/tableroulante",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 5, y = 15 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/shower",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 12 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 13 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Toilets",
            funcPrefix = "Toilets",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/toilets",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 15 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/lavaboDown",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 12},
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 13 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },

        --CHAMBRE 4
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/old_bed",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 26 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 2, y = 26 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TV",
            funcPrefix = "TV",
            funcPage = 1,
            anim = "data/Sprites/Accessories/TV",
            nbSprite = {9},
            animWait = 15,
            pos = { x = 3, y = 23 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "Perf",
            funcPrefix = "Perf",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/perfusion",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 25 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Table",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/tableroulante",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 5, y = 26 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/shower",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 25 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 26 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Toilets",
            funcPrefix = "Toilets",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/toilets",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 28 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/lavaboDown",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 25},
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 26 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },

        --CHAMBRE 5
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/old_bed",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 32 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 2, y = 32 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TV",
            funcPrefix = "TV",
            funcPage = 1,
            anim = "data/Sprites/Accessories/TV",
            nbSprite = {9},
            animWait = 15,
            pos = { x = 3, y = 29 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "Perf",
            funcPrefix = "Perf",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/perfusion",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 31 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Table",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/tableroulante",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 5, y = 32 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/shower",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 29 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 30 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Toilets",
            funcPrefix = "Toilets",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/toilets",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 32 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/lavaboDown",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 29},
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 30 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },

        --CHAMBRE 6
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/old_bed",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 38 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Bed",
            funcPrefix = "Bed",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 2, y = 38 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TV",
            funcPrefix = "TV",
            funcPage = 1,
            anim = "data/Sprites/Accessories/TV",
            nbSprite = {9},
            animWait = 15,
            pos = { x = 3, y = 35 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "Perf",
            funcPrefix = "Perf",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/perfusion",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 1, y = 37 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Table",
            funcPrefix = "Table",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/tableroulante",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 5, y = 38 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/shower",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 37 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Shower",
            funcPrefix = "Shower",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 38 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Toilets",
            funcPrefix = "Toilets",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/toilets",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 9, y = 40 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/lavaboDown",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 37},
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Lavabo",
            funcPrefix = "Lavabo",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 7, y = 38 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },


        --PLACARD A BALAI
        {
            name = "Balai",
            funcPrefix = "Balai",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/mop",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 28, y = 8 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },

        --SALLE DE REPOS
        {
            name = "Canape",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/canape",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 17, y = 11 },
            size = { w = 4, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "Canape",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 17, y = 12 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Canape",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 18, y = 11 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Canape",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 18, y = 12 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Canape",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 19, y = 11 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Canape",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 19, y = 12 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Canape",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 20, y = 11 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Canape",
            funcPrefix = "Canape",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 20, y = 12 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },

        {
            name = "Armchair",
            funcPrefix = "Armchair",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/armchair",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 25, y = 12},
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Armchair",
            funcPrefix = "Armchair",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/armchair",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 26, y = 12},
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Armchair",
            funcPrefix = "Armchair",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/armchair",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 27, y = 12},
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "Armchair",
            funcPrefix = "Armchair",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/armchair",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 28, y = 12},
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "TV",
            funcPrefix = "TV",
            funcPage = 1,
            anim = "data/Sprites/Accessories/TV",
            nbSprite = {8},
            animWait = 15,
            pos = { x = 26, y = 10 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "small_plant",
            funcPrefix = "small_plant",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/small_plant",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 23, y = 17 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "run",
            funcPrefix = "run",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/run",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 27, y = 17 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "run",
            funcPrefix = "run",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 28, y = 17 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "run",
            funcPrefix = "run",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/run",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 27, y = 16 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "run",
            funcPrefix = "run",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 28, y = 16 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true,
            run = true
        },{
            name = "run",
            funcPrefix = "run",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/run",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 27, y = 15 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true,
            run = true
        },
        {
            name = "run",
            funcPrefix = "run",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 28, y = 15 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true,
        },
        {
            name = "radio",
            funcPrefix = "radio",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/radio",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 19, y = 18 },
            size = { w = 2, h = 2 },
            dir = "down",
            block = true,
        },
        {
            name = "radio",
            funcPrefix = "radio",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 19, y = 19 },
            size = { w = 2, h = 2 },
            dir = "down",
            block = true,
        },
        {
            name = "radio",
            funcPrefix = "radio",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 0,
            pos = { x = 20, y = 19 },
            size = { w = 2, h = 2 },
            dir = "down",
            block = true,
        },

        -- ACCUEIL
        -- comptoire
        {
            name = "meuble",
            funcPrefix = "meuble",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/meuble_hopital",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 20, y = 4 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = false
        },
        {
            name = "meubleF",
            funcPrefix = "meuble",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 20, y = 5 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "meuble",
            funcPrefix = "meuble",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/meuble_hopital",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 21, y = 4 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = false
        },
        {
            name = "meubleF",
            funcPrefix = "meuble",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 21, y = 5 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },
        {
            name = "meuble",
            funcPrefix = "meuble",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/meuble_hopital",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 22, y = 4 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = false
        },
        {
            name = "meubleF",
            funcPrefix = "meuble",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 22, y = 5 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "meuble",
            funcPrefix = "meuble",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/meuble_hopital",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 23, y = 4 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = false
        },
        {
            name = "meubleF",
            funcPrefix = "meuble",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 23, y = 5 },
            size = { w = 1, h = 2 },
            dir = "down",
            block = true
        },

        --FINAL DOOR
        {
            name = "FinalDoor",
            funcPrefix = "FinalDoor",
            funcPage = 1,
            anim = "data/Sprites/Furnitures/finalDoor",
            nbSprite = {4},
            animWait = 30,
            pos = { x = 21, y = 0 },
            size = { w = 2, h = 1 },
            dir = "down",
            block = true
        },
        {
            name = "FinalDoorF",
            funcPrefix = "FinalDoor",
            funcPage = 1,
            anim = "",
            nbSprite = {4},
            animWait = 30,
            pos = { x = 22, y = 0 },
            size = { w = 1, h = 1 },
            dir = "down",
            block = true
        },

        -- MEDICS
        --Bloqueurs couloir gauche
        {
            name = "Medic",
            funcPrefix = "Medic",
            funcPage = 1,
            anim = "data/Sprites/Characters/NPCs/Medic/",
            nbSprite = {2, 4},
            animWait = 30,
            pos = { x = 10, y = 19 },
            size = { w = 1, h = 2 },
            dir = "right",
            speed = 4,
            run = true,
            block = false -- peut importe sa valeur sa tête est dans le mur qui bloque forcement
        },

        {
            name = "Medic",
            funcPrefix = "Medic",
            funcPage = 1,
            anim = "data/Sprites/Characters/NPCs/Medic/",
            nbSprite = {2, 4},
            animWait = 30,
            pos = { x = 10, y = 20 },
            size = { w = 1, h = 2 },
            dir = "right",
            speed = 4,
            run = true,
            block = true
        },
        {
            name = "MedicF",
            funcPrefix = "Medic",
            funcPage = 1,
            anim = "",
            nbSprite = {1},
            animWait = 30,
            pos = { x = 10, y = 21 },
            size = { w = 1, h = 2 },
            dir = "right",
            speed = 4,
            block = true
        },

        {
            name = "Medic",
            funcPrefix = "Medic",
            funcPage = 1,
            anim = "data/Sprites/Characters/NPCs/Medic/",
            nbSprite = {2, 4},
            animWait = 30,
            pos = { x = 21, y = 6 },
            size = { w = 1, h = 2 },
            dir = "up",
            speed = 4,
            run = true,
            block = false
        },
        {
            name = "MedicF",
            funcPrefix = "Medic",
            funcPage = 1,
            anim = "",
            nbSprite = {2, 4},
            animWait = 30,
            pos = { x = 21, y = 7 },
            size = { w = 1, h = 2 },
            dir = "up",
            speed = 4,
            run = true,
            block = true
        },



        {
            name = "Player",
            funcPrefix = "Player",
            funcPage = 1,
            anim = "data/Sprites/Characters/Player/Old/",
            nbSprite = {2, 4},
            animWait = 25,
            pos = { x = 1, y = 38 },
            size = { w = 1, h = 2 },
            dir = "down",
            speed = 1,
            run = true
        },
    },

    TVAction = function(ent, map)
        Music:Play("vieux-"..math.random(1,3)..".wav", false)
        table.insert(dialogList, "Moi: Pas le temps pour ces conneries !")
    end,

    FinalDoorCutscene = function(map, frame)
        local finalDoor = map:GetEntityByName("FinalDoor")
        if frame <= 20 then
            finalDoor.anim.state = 1
        elseif frame <= 40 then
            finalDoor.anim.state = 2
        elseif frame <= 60 then
            finalDoor.anim.state = 3
        elseif frame == 120 then
            finalDoor.entityData.block = false
            map:StopCutscene()
            gameState = CREDIT
        end

    end,

    FinalDoorAction = function(ent, map)
        map:StartCutscene("FinalDoorCutscene")
    end,

    PlayerUpdate = function(player, map)
        if not player.moving then
            if input.up == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "down") then
                player.entityData.dir = "up"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = 0
                player.moveOffsetY = -1
            elseif input.down == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "up") then
                player.entityData.dir = "down"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = 0
                player.moveOffsetY = 1
            elseif input.right == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "left") then
                player.entityData.dir = "right"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = 1
                player.moveOffsetY = 0
            elseif input.left == input.HELD and map:CanMove(player.entityData.pos.x, player.entityData.pos.y, "right") then
                player.entityData.dir = "left"
                player.anim.variation = 1
                player.moving = true
                player.moveOffsetX = -1
                player.moveOffsetY = 0
            else
                if input.up == input.HELD then
                    player.entityData.dir = "up"
                elseif input.down == input.HELD then
                    player.entityData.dir = "down"
                elseif input.left == input.HELD then
                    player.entityData.dir = "left"
                elseif input.right == input.HELD then
                    player.entityData.dir = "right"
                end
                player.anim.variation = 0
            end
        elseif player.moveOffsetX >= 16 or player.moveOffsetY >= 16 or player.moveOffsetX <= -16 or player.moveOffsetY <= -16 then
            player.moving = false
            if player.moveOffsetX > 0 then
                player.entityData.pos.x = player.entityData.pos.x + 1
            elseif player.moveOffsetX < 0 then
                player.entityData.pos.x = player.entityData.pos.x - 1
            elseif player.moveOffsetY > 0 then
                player.entityData.pos.y = player.entityData.pos.y + 1
            elseif player.moveOffsetY < 0 then
                player.entityData.pos.y = player.entityData.pos.y - 1
            end
            player.moveOffsetX = 0
            player.moveOffsetY = 0
        else
            --TODO: simplify logic
            if player.moveOffsetX > 0 then
                player.moveOffsetX = player.moveOffsetX + player.entityData.speed
            elseif player.moveOffsetX < 0 then
                player.moveOffsetX = player.moveOffsetX - player.entityData.speed
            elseif player.moveOffsetY > 0 then
                player.moveOffsetY = player.moveOffsetY + player.entityData.speed
            elseif player.moveOffsetY < 0 then
                player.moveOffsetY = player.moveOffsetY - player.entityData.speed
            end
        end
    end
}
