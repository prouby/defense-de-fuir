-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

return {
    musics = {
        "Intro.wav",
        "Baby.mp3",
        "Adult.mp3",
        "Old.mp3",
        "adulte-1.wav",
        "adulte-2.wav",
        "adulte-3.wav",
        "bebe-1.wav",
        "bebe-2.wav",
        "bebe-3.wav",
        "bebe-4.wav",
        "clic-menu-1.wav",
        "coup-bois-1.wav",
        "coup-bois-2.wav",
        "coup-bois-3.wav",
        "crac-bois-1.wav",
        "echec-1.wav",
        "fauteil-roulant-1.wav",
        "fauteil-roulant-2.wav",
        "fauteil-roulant-3.wav",
        "fouille-1.wav",
        "fouille-2.wav",
        "son-cle-1.wav",
        "son-cle-2.wav",
        "teddy-bear-1.wav",
        "teddy-bear-2.wav",
        "vieux-1.wav",
        "vieux-2.wav",
        "vieux-3.wav",
        "voiture-1.wav",
        "voiture-2.wav",
        "voiture-3.wav",
        "voiture-4.wav",
        "voiture-short-1.wav",
        "voiture-short-2.wav",
        "voiture-short-3.wav",
        "voiture-short-4.wav",
        "voix-1-a.wav",
        "voix-1-b.wav",
        "voix-1-c.wav",
        "voix-1-d.wav",
        "voix-2-a.wav",
        "voix-2-b.wav",
        "voix-2-c.wav",
        "voix-2-d.wav",
        "voix-int-a.wav",
        "voix-int-b.wav",
        "voix-non.wav",
        "voix-oui.wav",
        "porte-ferme-1.wav",
        "porte-ferme-2.wav",
        "porte-ouvre-1.wav",
        "porte-ouvre-2.wav",
        "eau-pas-1.wav",
        "eau-plouf-1.wav",
        "eau-plouf-2.wav",
    },

    sounds = {},

    currentMusic = nil,

    Load = function(Music)
        local sounds = {}

        for k, m in pairs(Music.musics) do
            local sound = love.audio.newSource("data/Sounds/"..m, "stream")
            sounds[m] = sound
        end

        Music.sounds = sounds
    end,

    Play = function (Music, k, loop)
        if Music.sounds[k] then
            Music.sounds[k]:setLooping(loop)

            love.audio.play(Music.sounds[k])
        end
    end,

    Stop = function (Music, k)
        if Music.sounds[k] then
            love.audio.stop(Music.sounds[k])
        end
    end,
}
