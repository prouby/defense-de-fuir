-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

return {
    Intro = function()
        if currentSound ~= "Intro.wav" then
            Music:Play("Intro.wav", true)
            Music.currentMusic = "Intro.wav"
        end

        introFrames = introFrames + 1

        love.graphics.draw(title_sprite, 0, 0, 0)
        love.graphics.setColor(0.79, 0.79, 0.79, 1)
        love.graphics.print("Jamming assembly 2019 !", 18, 152, -0.4)

        if introFrames > 120 then
            introFrames = 0
        end

        local alpha = 0
        if introFrames <= 60 then
            alpha = 1
        end

        love.graphics.setColor(1, 1, 1, alpha)
        love.graphics.print("Press start/space to play", 340, 420)

        love.graphics.setColor(1, 1, 1, 1)
    end,

    InGame = function()
        currentMap:Draw()
        -- Util.DrawDialogBox("Hello world!")
    end,

    Credit = function()
        love.graphics.print("Merci d'avoir joué !", 210, 180)
        
        love.graphics.print("Par PUTACLIC 2 - Le Retour", 155, 300)
        love.graphics.print("feat PUTACLIC.", 230, 340)
    end,
}
