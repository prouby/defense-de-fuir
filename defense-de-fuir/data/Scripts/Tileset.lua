-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

local Tileset = {

}
function Tileset:CanMove(tileID, dir)
    --print("tileset:", self.tileset[tileID].img)
    return not self.tileset[tileID].block and not (
        (dir == "up" and self.tileset[tileID].blockNorth) or
        (dir == "down" and self.tileset[tileID].blockSouth) or
        (dir == "right" and self.tileset[tileID].blockEast) or
        (dir == "left" and self.tileset[tileID].blockWest))
end
function Tileset.Create(mapname)
    local tileset = require("data.Scripts.Tilesets."..mapname)
    tileset.CanMove = Tileset.CanMove
    tileset.tilefolder = "data/Sprites/Tiles/"..mapname.."/"
    return tileset
end
return Tileset