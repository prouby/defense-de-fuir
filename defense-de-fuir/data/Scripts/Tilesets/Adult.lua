-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

return {
    tileset = {

        --ENVIRONMENT
        {--1 wooden floor
            img = "floor2"
        },
        {--2 stone floor
            img = "floor3"
        },
        {--3 white floor
            img = "white_floor"
        },
        {--4 blue wall
            block = true,
            img = "blue_wall"
        },
        {--5 bathroom wall
            block = true,
            img = "bathroom_wall"
        },
        {--6 brick wall
            block = true,
            img = "brick_wall"
        },
        {--7 horizontal hidden wall
            img = "hidden_wall",
            block = true
        },
        {--8 vertical right wall
            img = "right_vertical_wall",
            block = true
        },
        {--9 vertical left wall
            img = "left_vertical_wall",
            block = true
        },
        {--10 double vertical wall
            img = "vertical_walls",
            block = true
        },
        {--11 right corner wall
            img = "right_corner_wall",
            block = true
        },
        {--12 left corner wall
            img = "left_corner_wall",
            block = true
        },
        {--13 topped vertical wall
            img = "topped_vertical_walls",
            block = true
        },
        {--14 black tile
            img = "black_tile",
            block = true
        }--[[,
        {--east wall
            blockEast = true
        },
        {--south wall
            blockSouth = true
        },
        {--west wall
            blockWest = true
        },
        {--north wall
            blockNorth = true
        },
        {--background wall
            block = true
        },
        {--up left corner
            blockNorth = true
            blockWest = true
        },
        {--up right corner
            blockNorth = true
            blockEast = true
        },
        {--down left corner
            blockSouth = true
            blockWest = true
        },
        {--down right corner
            blockSouth = true
            blockEast = true
        },
        {--classic door

        },

        -- GARAGE
        {--ground
        },
        {--car 0,0
            block = true
        },
        {--car 1,0
            block = true
        },
        {--car 0,1
            block = true
        },
        {--car 1,1
            block = true
        },
        {--car 0,2
            block = true
        },
        {--car 1,2
            block = true
        },
        {--garage door

        },

        --KITCHEN
        {--ground
        },
        {--bar
            block = true
        },
        {--cooker
            block = true
        },

        --LIVING ROOM
        {--ground
        },
        {--chair up
            block = true
        },
        {--chair down
            block = true
        },
        {--little table
            block = true
        },

        --CHAMBER
        {--ground
        },
        {--night stand
            block = true
        },

        --BATH ROOM
        {--ground
        },
        {--bath
            block = true
        },
        {--sink
            block = true
        },
        {-- toilets
            block = true
        },]]--
    }
}
