-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

return {
    tileset = {
        {--1 ground
            img = "ground"
        },
        {--2 horizontal visible wall
            img = "horizontal_wall",
            block = true
        },
        {--3 horizontal hidden wall
            img = "hidden_wall",
            block = true
        },
        {--4 vertical right wall
            img = "right_vertical_wall",
            block = true
        },
        {--5 vertical left wall
            img = "left_vertical_wall",
            block = true
        },
        {--6 double vertical wall
            img = "vertical_walls",
            block = true
        },
        {--7 right corner wall
            img = "right_corner_wall",
            block = true
        },
        {--8 left corner wall
            img = "left_corner_wall",
            block = true
        },
        {--9 topped vertical wall
            img = "topped_vertical_walls",
            block = true
        },
        {--10 black tile
            img = "black_tile",
            block = true
        }
    }
}
