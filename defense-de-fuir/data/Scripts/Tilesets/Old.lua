-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

return {
    tileset = {
        --ENVIRONMENT
        {--1 basic hospital floor
            img = "hospital_tile"
        },
        {--2 floor2 (no sprite yet : TODO)
            img = ""
        },
        {--3 floor3 (no sprite yet : TODO)
            img = ""
        },
        {--4 hospital wall
            block = true,
            img = "hospital_wall_0"
        },
        {--5 hospital wall with a crack
            block = true,
            img = "hospital_wall_1"
        },
        {--6 classic wall
            block = true,
            img = "blue_wall"
        },
        {--7 horizontal hidden wall
            img = "hidden_wall",
            block = true
        },
        {--8 vertical right wall
            img = "right_vertical_wall",
            block = true
        },
        {--9 vertical left wall
            img = "left_vertical_wall",
            block = true
        },
        {--10 double vertical wall
            img = "vertical_walls",
            block = true
        },
        {--11 right corner wall
            img = "right_corner_wall",
            block = true
        },
        {--12 left corner wall
            img = "left_corner_wall",
            block = true
        },
        {--13 topped vertical wall
            img = "topped_vertical_walls",
            block = true
        },
        {--14 black tile
            img = "black_tile",
            block = true
        }
    }
}
