-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

return {
    -- Returns the reverse of the given direction
    GetReverseDir = function(dir)
        if dir == "up"    then return "down"  end
        if dir == "down"  then return "up"    end
        if dir == "left"  then return "left"  end
        if dir == "right" then return "right" end
        return ""
    end,

    Split = function(str)
        return string.gmatch(str, " ")
    end,

    DrawDialogBox = function(str)
        local oldR, oldG, oldB, oldA = love.graphics.getColor()
        love.graphics.setColor(0.18, 0.18, 0.18, 0.8)

        -- Draw dialog box
        love.graphics.polygon('fill', 0, 400, 0, 480, 640, 480, 640, 400)

        -- Text
        love.graphics.setColor(1, 1, 1, 1)
        love.graphics.print(str, 40, 420)

        -- Restore color
        love.graphics.setColor(oldR, oldG, oldB, oldA)
    end,

    --inserts the content of the source table into the destination table (subtables are not duplicated)(identical keys are overwritten)
    InsertTable = function(dst, src)
        for k, v in pairs(src) do
            dst[k] = v
        end
        return dst
    end,

    UpdateInputs = function(input)
        for k, v in pairs(input) do
            if k ~= "IDLE" and k ~= "PRESSED" and k ~= "HELD" and k ~= "RELEASED" then
                if v == input.PRESSED  then input[k] = input.HELD end
                if v == input.RELEASED then input[k] = input.IDLE end
            end
        end
    end,
}
