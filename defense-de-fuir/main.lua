-- Copyright (C) 2019 by Pierre-Antoine ROUBY
-- Copyright (C) 2019 by Nicolas MARIN-PACHE
-- Copyright (C) 2019 by Pamphile SALTEL
-- Copyright (C) 2019 by Rhenaud DUBOIS
-- Copyright (C) 2019 by Pauline WARGNY

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

--
-- Mouse
--

input = {
    IDLE = 0,
    PRESSED = 1,
    HELD = 2,
    RELEASED = 3,

    left = 0,
    right = 0,
    up = 0,
    down = 0,
    action = 0,
    back = 0,
    pause = 0,
    endLevel = 0,
}

function love.mousereleased (x, y, button)
    if button == 1 then
        -- TODO
    end
end

--
-- Keys
--

function love.keypressed (key, scancode)
    -- Stop any input if there's a cutscene
    if currentMap and currentMap:HasCutscene() then
        if key == "o" then currentMap:StopCutscene()
        else               return
        end
    end

    if key == "space" then         if input.action == input.IDLE then input.action = input.PRESSED end
    elseif key == "z" then         if input.up     == input.IDLE then input.up     = input.PRESSED end
    elseif key == "q" then         if input.left   == input.IDLE then input.left   = input.PRESSED end
    elseif key == "s" then         if input.down   == input.IDLE then input.down   = input.PRESSED end
    elseif key == "d" then         if input.right  == input.IDLE then input.right  = input.PRESSED end
    elseif key == "backspace" then if input.back   == input.IDLE then input.back   = input.PRESSED end
    elseif key == "p" then         if input.pause  == input.IDLE then input.pause  = input.PRESSED end
    elseif key == "o" then         if input.endLevel  == input.IDLE then input.endLevel  = input.PRESSED end
    end
end

function love.keyreleased (key, scancode)
    if key == "space" then         input.action = input.RELEASED
    elseif key == "z" then         input.up     = input.RELEASED
    elseif key == "q" then         input.left   = input.RELEASED
    elseif key == "s" then         input.down   = input.RELEASED
    elseif key == "d" then         input.right  = input.RELEASED
    elseif key == "backspace" then input.back   = input.RELEASED
    elseif key == "p" then         input.pause  = input.RELEASED
    elseif key == "escape" then    love.event.quit()
    elseif key == "o" then      input.endLevel = input.RELEASED
    end
end

--
-- Gamepad
--

function love.gamepadpressed (joystick, button)
    -- Stop any input if there's a cutscene
    if currentMap and currentMap:HasCutscene() then
        if key == "o" then currentMap:StopCutscene()
        else               return
        end
    end
    if button == "a" then            if input.action == input.IDLE then input.action = input.PRESSED end
    elseif button == "dpleft" then   if input.up     == input.IDLE then input.up     = input.PRESSED end
    elseif button == "dpright" then  if input.left   == input.IDLE then input.left   = input.PRESSED end
    elseif button == "dpup" then     if input.down   == input.IDLE then input.down   = input.PRESSED end
    elseif button == "dpdown" then   if input.right  == input.IDLE then input.right  = input.PRESSED end
    elseif button == "b" then        if input.back   == input.IDLE then input.back   = input.PRESSED end
    elseif button == "start" then    if input.pause  == input.IDLE then input.pause  = input.PRESSED end
    end
end

function love.gamepadreleased (joystick, button)
    if button == "a" then            input.action = input.RELEASED
    elseif button == "dpleft" then   input.left   = input.RELEASED
    elseif button == "dpright" then  input.right  = input.RELEASED
    elseif button == "dpup" then     input.up     = input.RELEASED
    elseif button == "dpdown" then   input.down   = input.RELEASED
    elseif button == "b" then        input.back   = input.RELEASED
    elseif button == "start" then    input.pause  = input.RELEASED
    elseif button == "select" then   love.event.quit()
    end
end

--
-- Load
--

function love.load()
    local major, minor, revision, codename = love.getVersion()
    if major ~= 11 then
        print ("-----------------------------------------------------------------")
        print (" Warning: Your LÖVE version is no the devlopper version: " .. major .. "." .. minor)
        print (" Warning: Use version: " .. 11 .. "." .. 3 .. " for full compatibility.")
        print ("-----------------------------------------------------------------")
    end

    -- No antialiasing
    love.graphics.setDefaultFilter("nearest", "nearest", 1)

    -- Game state
    INTRO = 1
    INGAME = 2
    CREDIT = 3

    gameState = INTRO
    -- Title font
    font = love.graphics.newFont("data/font/Delius-Regular.ttf", 24)
    love.graphics.setFont (font)

    -- TODO assets
    title_sprite = love.graphics.newImage("data/title_back_ground.png")

    -- Useful objects
    Util = require "data/Scripts/Util"
    Map = require "data/Scripts/Map"
    Tileset = require "data/Scripts/Tileset"
    Render = require "data/Scripts/Render"
    Entity = require "data/Scripts/Entity"
    Music = require "data/Scripts/Music"

    -- Load maps
    maps = {
        Baby = Map.Create("Baby"),
        Adult = Map.Create("Adult"),
        Old = Map.Create("Old"),
    }

    currentMap = maps["Baby"]

    -- Fps
    frameTime = 1/60
    startTime = love.timer.getTime()

    -- Intro vars
    introFrames = 0

    -- Music
    Music:Load()

    -- Dialog
    dialogList = {}
    table.insert(dialogList, "")
    table.insert(dialogList, "Tuto: 'z' 'q' 's' 'd' pour bouger.")
    table.insert(dialogList, "Tuto: 'space' pour interagir.")
end

--
-- Draw
--

function love.draw()
    if input.action == input.PRESSED and gameState == INTRO then
        gameState = INGAME
    end

    if gameState == INTRO then
        Render.Intro()
    elseif gameState == INGAME then
        Render.InGame()
    elseif gameState == CREDIT then
        Render.Credit()
    end
    Util.UpdateInputs(input)

    endTime = love.timer.getTime()
    if endTime < (startTime + frameTime) then
        love.timer.sleep(frameTime - (endTime - startTime))
    end
    startTime = love.timer.getTime()
end
